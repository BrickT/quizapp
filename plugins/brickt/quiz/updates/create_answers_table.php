<?php namespace Brickt\Quiz\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateAnswersTable extends Migration
{
    public function up()
    {
        Schema::create('brickt_quiz_answers', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('answer')->nullable();
            $table->boolean('correct');
            $table->integer('question_id')->unsigned()->nullable()->index();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('brickt_quiz_answers');
    }
}
