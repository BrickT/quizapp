<?php namespace Brickt\Quiz\Models;

use Model;

/**
 * Question Model
 */
class Question extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'brickt_quiz_questions';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'answers' => ['Brickt\Quiz\Models\Answer']
    ];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
