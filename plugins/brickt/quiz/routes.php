<?php 

use Brickt\Quiz\Models\Question;


Route::get('api/quiz', function() {
    $questions = Question::all();
    
    foreach ($questions as $key_q=>$value_q) {
        $quiz_arr[$key_q]['question'] = $value_q->question;
        $quiz_arr[$key_q]['q_id'] = $value_q->id;
        $quiz_arr[$key_q]['answers'] = getAnswers($value_q->answers);
    }
    return $quiz_arr;
});



  function getAnswers($answers) {
    foreach ($answers as $key_a => $value_a) {
        $answer_arr[$key_a]['answer'] = $value_a->answer;
        $answer_arr[$key_a]['a_id'] = $value_a->id;
        $answer_arr[$key_a]['correct'] = $value_a->correct;
    }
    return  $answer_arr;
}