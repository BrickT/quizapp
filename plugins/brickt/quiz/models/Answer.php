<?php namespace Brickt\Quiz\Models;

use Model;

/**
 * Answer Model
 */
class Answer extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'brickt_quiz_answers';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'question' => ['Brickt\Quiz\Models\Question']
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
