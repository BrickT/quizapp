<?php namespace Brickt\Quiz;

use Backend;
use System\Classes\PluginBase;

/**
 * quiz Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Quiz',
            'description' => 'Quiz app',
            'author'      => 'brickt',
            'icon'        => 'icon-question'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Brickt\Quiz\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'brickt.quiz.some_permission' => [
                'tab' => 'quiz',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'quiz' => [
                'label'       => 'quiz',
                'url'         => Backend::url('brickt/quiz/questions'),
                'icon'        => 'icon-question',
                'permissions' => ['brickt.quiz.*'],
                'order'       => 500,

            ],
        ];
    }
}
